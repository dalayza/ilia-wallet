import mongoose from 'mongoose';

const options = {
    timestamps: { createdAt: true, updatedAt: false }
};

const WalletSchema = new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    func: { type: String, required: true },
    company: { type: String, required: true },
    register: { type: Boolean, required: true, default: false }
}, options);

export { WalletSchema };

export default mongoose.model('wallet', WalletSchema);