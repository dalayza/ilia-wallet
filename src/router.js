import bodyParser from 'body-parser';

import { zipkinMiddleware as zipkin } from './safe/zipkin';
import { getUsers, postUser, updateUsers, deleteUser, findUserById, shipmentUser } from './controllers/mongoDb';

export default app => {
    app.get('/wallet/health', (req, res) => res.send({ status: 'UP' }));

    app.get('/hello', (req, res) => res.status(200).send('Hello, wallet service!'));

    app.get('/users', bodyParser.json(), zipkin(), getUsers);

    app.post('/users', bodyParser.json(), zipkin(), postUser);

    app.put('/users', bodyParser.json(), zipkin(), updateUsers);

    app.delete('/users', bodyParser.json(), zipkin(), deleteUser);

    app.get('/findUsers', bodyParser.json(), zipkin(), findUserById);
};