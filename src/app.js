import express from 'express';
import http from 'http';

import config from './safe/config';
import logger from './safe/logger';
import eureka from './safe/eureka';
import router from './router';
import mongodb from './controllers/mongodb';
// import redis from './controllers/redis';

let server = null;

const initServer = async() => {
    try {
        logger.setAppName(config.getAppName());

        // Cloud Config
        await config.load();

        if (!config.get()) {
            logger.error('Could not load config file from server!');
            return;
        }

        // Eureka
        logger.info('Eureka registry starting...');
        await eureka.register();

        if (!eureka.client()) {
            logger.error('Could not register at Eureka!');
            return;
        }

        logger.info('Eureka registry UP');

        // Server setup
        const app = express();
        const PORT = config.get(11010);
        const HOSTNAME = '0.0.0.0';
        const APPNAME = config.get('insurance-wallet-service');

        server = http.createServer(app);
        server.close = Promise.promisify(server.close);

        router(app);

        server.listen(PORT, HOSTNAME, () => logger.info(`${APPNAME} STARTED on ${HOSTNAME}:${PORT}`));

        // Redis Connection
        // redis.connect();

        // MongoDB Connection
        mongodb.connect();
    } catch (err) {
        logger.error(err);
        process.kill(process.pid, 'SIGINT');
    }
};

initServer();

// Graceful shutdown

const shutdown = async type => {
    try {
        logger.warn(`Receive ${type} signal`);

        // await redis.disconnect();
        await mongodb.disconnect();

        if (await eureka.deregister()) {
            logger.warn('Eureka deregistered');
        }

        if (server) {
            await server.close();
            logger.warn('Server closed');
        }

        logger.warn('Graceful EXIT');
    } catch (err) {
        logger.error(err);
    } finally {
        setTimeout(() => process.exit(), 3000); // Delay to finish log
    }
};

process.on('SIGINT', () => shutdown('SIGINT'));
process.on('SIGTERM', () => shutdown('SIGTERM'));