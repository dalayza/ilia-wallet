import mongoose from 'mongoose';

import config from '../safe/config';
import logger from '../safe/logger';
import { WalletSchema } from '../models/Wallet';

let walletConn = null;
let Wallet = null;

const CONNECTION_OPTIONS = {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    keepAliveInitialDelay: 300000, // is the number of milliseconds to wait before initiating keepAlive on the socket
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    connectTimeoutMS: 60000, // Give up initial connection after 60 seconds
    socketTimeoutMS: 60000, // Close sockets after 60 seconds of inactivity
};

let RETRY_DELAY = 5000;

const connect = async() => connectWalletDb();

const connectWalletDb = async() => {
    const DB_USERNAME = config.get('mongodb.instance.user');
    const DB_PASSWORD = config.get('mongodb.instance.password');
    const DB_HOST = config.get('mongodb.instance.hostname');
    const DB_PORT = config.get('mongodb.instance.port');
    const DB_DATABASE = config.get('mongodb.instance.database');

    const DATABASE_URI = `mongodb://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_DATABASE}`;
    logger.info(`MONGODB Connecting mongodb://${DB_HOST}:${DB_PORT}/${DB_DATABASE}`);

    try {
        mongoose.pluralize(null);
        walletConn = await mongoose.createConnection(DATABASE_URI, CONNECTION_OPTIONS)
            .on('connected', () => logger.info(`MONGODB connection stablished: ${DB_HOST}:${DB_PORT}/${DB_DATABASE}`))
            .on('disconnected', () => {
                logger.warn(`MONGODB disconnected: ${DB_HOST}:${DB_PORT}/${DB_DATABASE}`);

                if (RETRY_DELAY) {
                    setTimeout(connectWalletDb, RETRY_DELAY);
                }
            });

        Wallet = walletConn.model('wallet', WalletSchema);
    } catch (err) {
        logger.error(`Failed to connect to ${DB_DATABASE}: ${err}`);
    }
};

const disconnect = async() => {
    logger.info('MONGODB Disconnecting...');
    RETRY_DELAY = 0;

    if (walletConn) {
        await walletConn.close();
    }
};

export default {
    connect,
    disconnect
};

export const getUsers = async ean => (
    new Promise((resolve, reject) => {
        logger.trace(`<MongoDB> Fetch wallet ${id}.`);

        if (!(mongoose.Types.ObjectId.isValid(id))) { return resolve(null); }
        Wallet
            .findById(id)
            .exec()
            .then(wallet => {
                if (wallet) {
                    logger.info(`<MongoDB> Wallet retrieved for Id ${id}.`);
                } else {
                    logger.info(`<MongoDB> Wallet not found for Id ${id}.`);
                }
                resolve(wallet);
            })
            .catch(reject);
    })
);

export const postUser = (username, func, company) => (
    new Promise((resolve, reject) => {
        Wallet.post()
            .then(() => (
                new Wallet(username, func, company)
                .save((err, wallet) => {
                    if (err) { return reject(err); }

                    resolve(wallet);
                })
            ))
            .catch(err => (
                reject(err)
            ));
    })
);

export const updateUsers = (func, company, register, shipment) => (
    new Promise((resolve, reject) => {
        Wallet.deleteMany({ ean })
            .then(() => (
                new Wallet(func, company, register, shipment)
                .save((err, wallet) => {
                    if (err) { return reject(err); }

                    resolve(wallet);
                })
            ))
            .catch(err => (
                reject(err)
            ));
    })
);

export const deleteUser = ean => (Wallet.deleteMany({ id }).exec());

export const findUserById = id => (
    new Promise((resolve, reject) => {
        logger.trace(`<MongoDB> Fetch wallet ${id}.`);

        if (!(mongoose.Types.ObjectId.isValid(id))) { return resolve(null); }
        Wallet
            .findById(id)
            .exec()
            .then(wallet => {
                if (wallet) {
                    logger.info(`<MongoDB> Wallet retrieved for Id ${id}.`);
                } else {
                    logger.info(`<MongoDB> Wallet not found for Id ${id}.`);
                }
                resolve(wallet);
            })
            .catch(reject);
    })
);