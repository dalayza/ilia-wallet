import axios from 'axios';
import bunyan from 'bunyan';

let APPNAME = 'microservice';
let LOGSTASH_URL = null;

// Override console

console.authorizedOutput = console.log;
console.log = msg => logger.trace(msg);
console.trace = msg => logger.trace(msg);
console.debug = msg => logger.trace(msg);
console.info = msg => logger.trace(msg);
console.warn = msg => logger.trace(msg);
console.error = msg => logger.trace(msg);

// Streams

function ConsoleStream() {}
ConsoleStream.prototype.write = data => {
    console.authorizedOutput(`${formatDateTime(data.time)} ${mapLevelToName(data.level)} ${data.msg}`);
};

function LogstashStream() {}
LogstashStream.prototype.write = data => {
    if (!LOGSTASH_URL) {
        return;
    }

    const message = {...data, level: mapLevelToName(data.level) };

    axios.post(LOGSTASH_URL, message)
        .catch(error => fileLogger.error(`Logstash error: ${error}`));
};

// Config

const consoleStreamConfig = {
    level: 'trace',
    type: 'raw',
    stream: new ConsoleStream()
};

const fileStreamConfig = {
    level: 'debug',
    type: 'rotating-file',
    path: './log/app.log',
    period: '1d',
    count: 3
};

const logstashStreamConfig = {
    level: 'info',
    type: 'raw',
    stream: new LogstashStream()
};

// Loggers

let fileLogger = bunyan.createLogger({
    name: APPNAME,
    streams: [
        consoleStreamConfig,
        fileStreamConfig
    ]
});

let logger = bunyan.createLogger({
    name: APPNAME,
    streams: [
        consoleStreamConfig,
        fileStreamConfig,
        logstashStreamConfig
    ]
});

const setAppName = appname => {
    if (appname) {
        APPNAME = appname;

        fileLogger = createLogger(APPNAME, [consoleStreamConfig, fileStreamConfig]);
        logger = createLogger(APPNAME, [consoleStreamConfig, fileStreamConfig, logstashStreamConfig]);
    }
};

const setLogService = logstash => {
    if (logstash && logstash.hostName && logstash.port && logstash.port.$) {
        LOGSTASH_URL = `http://${logstash.hostName}:${logstash.port.$}`;
        console.authorizedOutput(`LOG configured to URL: ${LOGSTASH_URL}`);
    }
};

export default {
    trace: msg => logger.trace(msg),
    debug: msg => logger.debug(msg),
    info: msg => logger.info(msg),
    warn: msg => logger.warn(msg),
    error: msg => logger.error(msg),
    fatal: msg => logger.fatal(msg),
    setAppName,
    setLogService
};

// Utils

const createLogger = (name, streams) => bunyan.createLogger({ name, streams });

const formatDateTime = time => {
    const year = time.getFullYear();
    const month = pad(time.getMonth() + 1, 2);
    const day = pad(time.getDate(), 2);
    const hour = pad(time.getHours(), 2);
    const minutes = pad(time.getMinutes(), 2);
    const seconds = pad(time.getSeconds(), 2);
    const milliseconds = pad(time.getMilliseconds(), 3);

    return `${day}-${month}-${year} ${hour}:${minutes}:${seconds}.${milliseconds}`;
};

const pad = (number, size = 0) => {
    let s = String(number);

    while (s.length < size) {
        s = `0${s}`;
    }

    return s;
};

const mapLevelToName = level => {
    switch (level) {
        case bunyan.TRACE:
            return 'TRACE';
        case bunyan.DEBUG:
            return 'DEBUG';
        case bunyan.INFO:
            return 'INFO';
        case bunyan.WARN:
            return 'WARN';
        case bunyan.ERROR:
            return 'ERROR';
        case bunyan.FATAL:
            return 'FATAL';
        default:
            return level;
    }
};