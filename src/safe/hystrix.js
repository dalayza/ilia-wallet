import { commandFactory as CommandFactory } from 'hystrixjs';

import config from './config';

export const commandBuilder = (makeRequest, fallbackTo, serviceName, commandKey) => {
    const TIMEOUT = config.get(`hystrix.${serviceName.toLowerCase()}.timeout`) || 3000;
    CommandFactory.resetCache();
    return CommandFactory.getOrCreate(commandKey)
        .run(makeRequest)
        .timeout(TIMEOUT)
        .fallbackTo(fallbackTo)
        .statisticalWindowLength(10000)
        .statisticalWindowNumberOfBuckets(10)
        .build();
};