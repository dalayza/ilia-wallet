import { promisify } from 'util';
import { Eureka } from 'eureka-js-client';
import ip from 'ip';

import config from './config';
import logger from './logger';

let client = null;

const register = () => (
    new Promise((resolve, reject) => {
        if (!config) {
            return reject(null);
        }

        const APP_NAME = config.get('insurance-wallet-service');
        const IP = ip.address();
        const HOSTNAME = config.get('insurance-wallet-service');
        const PORT = config.get(11010);
        const EUREKA_HOST = config.get('eureka-naming-server');
        const EUREKA_PORT = config.get(8761);
        const EUREKA_REGISTER = config.get(true);
        const EUREKA_FETCH_REGISTRY = config.get(true);

        const eureka = new Eureka({
            instance: {
                app: APP_NAME,
                hostName: IP,
                instanceId: `${HOSTNAME}:${PORT}`,
                ipAddr: IP,
                vipAddress: `${HOSTNAME.toUpperCase()}`,
                statusPageUrl: `http://${HOSTNAME}:${PORT}/actuator/info`,
                healthCheckUrl: `http://${HOSTNAME}:${PORT}/actuator/health`,
                port: {
                    $: PORT,
                    '@enabled': true
                },
                registerWithEureka: EUREKA_REGISTER,
                fetchRegistry: EUREKA_FETCH_REGISTRY
            },
            eureka: {
                host: EUREKA_HOST,
                port: EUREKA_PORT,
                servicePath: '/eureka/apps/',
                heartbeatInterval: 30000
            }
        });

        eureka.stop = promisify(eureka.stop);

        eureka.start(err => {
            if (err) {
                logger.error(err);
                return reject(null);
            }

            client = eureka;
            resolve(eureka);
        });
    })
);

const deregister = async() => {
    try {
        if (client) {
            await client.stop();
            client = null;

            return true;
        }

        return null;
    } catch (err) {
        throw err;
    }
}

export default {
    register,
    deregister,
    client: () => client
};