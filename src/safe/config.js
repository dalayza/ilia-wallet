import client from 'cloud-config-client';
import fs from 'fs';
import path from 'path';
import YAML from 'yaml';
import logger from "./logger";
import InsuranceWalletService from '../config/insurance-wallet-service.yml';

let appname = 'microservice';
let cloudConfigUri = null;
let config = null;

const init = () => {
    try {
        logger.info('Load bootstrap configs...');

        const filePath = path.join(__dirname, InsuranceWalletService);
        const file = fs.readFileSync(filePath, 'utf8');
        const yamlFile = YAML.parse(file);

        cloudConfigUri = yamlFile;
        appname = yamlFile.app.name;
    } catch (err) {
        logger.error(err);
    }
}

init();

const load = async() => {
    await fetch();

    return;
};

const fetch = async() => {
    try {
        config = await client.load({ endpoint: cloudConfigUri, application: appname });
    } catch (err) {
        logger.error(err);
        config = null;
    }

    return config;
};

const get = property => (property ? config._properties[property] : config);

export default {
    getAppName: () => appname,
    load,
    fetch,
    get
};