import { BatchRecorder, jsonEncoder, Tracer, Instrumentation } from 'zipkin';
import { expressMiddleware, wrapExpressHttpProxy } from 'zipkin-instrumentation-express';
import { HttpLogger } from 'zipkin-transport-http';
import axios from 'axios';
import proxy from 'express-http-proxy';
import CLSContext from "zipkin-context-cls";

import config from './config';

let tracer = null;

export const zipkinMiddleware = () => {
    const serviceName = config.get('insurance-wallet-service');
    const endpoint = config.get('http://zipkin:9411/api/v2/spans');
    const ctxImpl = new CLSContext();

    const recorder = new BatchRecorder({
        logger: new HttpLogger({
            endpoint: endpoint,
            jsonEncoder: jsonEncoder.JSON_V2,
        })
    });

    tracer = new Tracer({ ctxImpl, recorder, localServiceName: serviceName });

    zipkinAxiosInterceptorsSetup(serviceName);

    return expressMiddleware({
        tracer,
        serviceName
    });

};

const zipkinAxiosInterceptorsSetup = (serviceName) => {

    let remoteServiceName = null;
    let instrumentation = null;

    const zipkinRecordRequest = (config) => {
        return tracer.scoped(() => {
            const url = new URL(config.url);
            const searchParams = new URLSearchParams(url.search);
            remoteServiceName = (searchParams.get('serviceName') || url.hostname || 'unknown');
            instrumentation = new Instrumentation.HttpClient({ tracer, serviceName, remoteServiceName });
            let newConfig = instrumentation.recordRequest(config, config.url, config.method);
            newConfig.traceId = tracer.id;
            return newConfig;
        });
    };

    const zipkinRecordResponse = (res) => {
        return tracer.scoped(() => {
            instrumentation.recordResponse(res.config.traceId, res.status);
            return res;
        });
    };

    const zipkinRecordError = (error) => {
        return tracer.scoped(() => {
            if (error.config) {
                const traceId = error.config.traceId;
                if (error.response) {
                    instrumentation.recordResponse(traceId, error.response.status);
                } else {
                    instrumentation.recordError(traceId, error);
                }
            }
            return Promise.reject(error);
        });
    };

    axios.interceptors.request.use(zipkinRecordRequest, zipkinRecordError);
    axios.interceptors.response.use(zipkinRecordResponse, zipkinRecordError);

};

export const zipkinProxy = (host, remoteServiceName) =>
    wrapExpressHttpProxy(proxy(host), { tracer, remoteServiceName });