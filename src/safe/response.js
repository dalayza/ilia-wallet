export class ResponseError {
    constructor({ status = 500, message }) {
        this.status = status;
        this.message = message;
    }
};

export const sendResponse = ({ status = 200, message, data }, res) => (
    res.status(status).send({
        status,
        message: message || getDefaultMessage(status),
        data
    })
);

export const sendError = ({ status = 500, message, data }, res) => (
    sendResponse({ status, message, data }, res)
);

const getDefaultMessage = status => {
    switch (status) {
        case 200:
            return 'Success';
        case 400:
            return 'Bad Request';
        case 401:
            return 'Unauthorized';
        case 404:
            return 'Requested resource could not be found';
        case 409:
            return 'Requested operation could not be fulfilled';
        case 422:
            return 'Mandatory parameters were not supplied';
        case 500:
            return 'Internal server error';
        default:
            return 'Default';
    }
};