require('core-js/stable');
require('regenerator-runtime/runtime');

global.Promise = require('bluebird');
Promise.coroutine.addYieldHandler(value => Promise.resolve(value));

// Babel hook to transpile on-the-fly
if (process.env.NODE_ENV !== 'production') {
    require('@babel/register'); // eslint-disable-line import/no-extraneous-dependencies
}

// App
require('./app');