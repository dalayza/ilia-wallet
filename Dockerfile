FROM node:10-alpine

WORKDIR /usr/src/app

RUN mkdir log

COPY --from=appbuild /usr/src/app/node_modules node_modules
COPY --from=appbuild /usr/src/app/build src

ENV NODE_ENV=production

RUN apk add --no-cache tzdata
ENV TZ America/Sao_Paulo

CMD node src/index.js
